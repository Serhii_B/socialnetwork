from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager
from flask import jsonify, request
from flask_jwt_extended import jwt_required, get_jwt_claims, create_access_token
from http import HTTPStatus
from datetime import datetime
import hashlib


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///dev.db'
app.config['JWT_SECRET_KEY'] = 'super-secret'
db = SQLAlchemy(app)
jwt = JWTManager(app)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(15))
    last_name = db.Column(db.String(15))
    username = db.Column(db.String(20), unique=True, nullable=False)
    password = db.Column(db.String(50), nullable=False)
    last_logged_in = db.Column(db.DateTime, default=None)
    last_action = db.Column(db.DateTime, default=None)
    post = db.relationship('Post', backref='user')


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.Text(500))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    post_like = db.relationship('PostLike', backref='post')


class PostLike(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    post_id = db.Column(db.Integer, db.ForeignKey('post.id'))
    date_liked = db.Column(db.DateTime, default=datetime.now())


class Md5PasswordHasher:
    def __init__(self, salt):
        self.salt = salt

    def hash(self, password):
        return hashlib.md5((password + self.salt).encode()).hexdigest()

    def verify(self, password, hashed_password):
        return self.hash(password) == hashed_password


hasher = Md5PasswordHasher('UOzQy')


def record_last_action(claims):
    try:
        db.session.query(User).filter_by(id=claims['user_id']) \
            .update({"last_action": datetime.now()}, synchronize_session=False)
        db.session.commit()
    except Exception:
        return False
    return True


jwt.claims_verification_loader(record_last_action)


@jwt.user_claims_loader
def add_claims_to_access_token(identity):
    return {"user_id": identity}


@app.route('/users/login', methods=['Post'])
def login():
    """
    Function logs in user by username and password
    :return: token with http status
    :rtype: json
    """
    if not request.is_json:
        return jsonify(message="Missing JSON in request"), HTTPStatus.BAD_REQUEST

    username = request.json.get('username')
    password = request.json.get('password')

    if not username:
        return jsonify(message="Missing username parameter"), HTTPStatus.BAD_REQUEST
    if not password:
        return jsonify(message="Missing password parameter"), HTTPStatus.BAD_REQUEST

    user = User.query.filter_by(username=username).first()

    if user is None or not hasher.verify(password, user.password):
        return jsonify(message="Bad username or password"), HTTPStatus.UNAUTHORIZED

    access_token = create_access_token(identity=user.id)
    if access_token:
        db.session.query(User).filter_by(id=user.id)\
            .update({"last_logged_in": datetime.now()}, synchronize_session=False)
        db.session.commit()
        return jsonify(access_token=access_token), HTTPStatus.OK
    else:
        return jsonify(message="Could not generate access token")


@app.route('/users', methods=['Post'])
def sing_up():
    """
    Function creates user by first_name, last_name, username, password
    :return: message about successful registration and http status
    :rtype: json
    """
    if not request.is_json:
        return jsonify(message="Missing JSON in request"), HTTPStatus.BAD_REQUEST

    first_name = request.json.get('first_name')
    last_name = request.json.get('last_name')
    username = request.json.get('username')
    password = request.json.get('password')
    pw_hash = hasher.hash(password)

    user = User(username=username, password=pw_hash,
                last_name=last_name, first_name=first_name)
    db.session.add(user)
    db.session.commit()
    if user:
        return jsonify(message="You were registered"), HTTPStatus.CREATED
    else:
        return jsonify(message="ERROR")


@app.route('/posts', methods=['Post'])
@jwt_required
def create_post():
    """
    This function make a record into the database adding post
    :return: post content with http status
    :rtype: json
    """
    user_id = get_jwt_claims()["user_id"]

    content = request.json.get('content')
    post = Post(content=content, user_id=user_id)
    db.session.add(post)
    db.session.commit()

    return jsonify(content=content), HTTPStatus.CREATED


@app.route('/posts/<int:post_id>', methods=['Get'])
def get_post(post_id):
    """
    This function returns post object from the database.
    :param post_id: Unique post identifier
    :type post_id: int
    :return: post object and http status
    :rtype: json
    """
    post = Post.query.get(post_id)

    if post:
        show_post = {"Post_ID": post.id, "Post_Content": post.content,
                     "User_id": post.user_id, "Likes": post.post_like}
        return show_post, HTTPStatus.OK
    else:
        return jsonify(message="error")


@app.route('/posts/<int:post_id>/like', methods=["Post"])
@jwt_required
def like_post(post_id):
    """
    Function adds like record to database
    :param post_id: Unique post identifier
    :type post_id: int
    :return: message about successful operation and http status
    :rtype: json
    """
    user_id = get_jwt_claims()["user_id"]

    check_post_exist = Post.query.filter(Post.id == post_id).first()
    if check_post_exist is None:
        return jsonify(message="No such post")

    liked_post = PostLike.query.filter_by(post_id=post_id, user_id=user_id).first()
    if liked_post is None:
        add_like = PostLike(post_id=post_id, user_id=user_id)
        db.session.add(add_like)
        db.session.commit()
        return jsonify(message="Like was added"), HTTPStatus.OK
    else:
        return jsonify(message="You already liked this post")


@app.route('/posts/<int:post_id>/unlike', methods=["Post"])
@jwt_required
def unlike_post(post_id):
    """
    Function deletes like record from the database
    :param post_id: Unique post identifier
    :type post_id: Unique post identifier
    :return: message about successful operation and http status
    :rtype: json
    """
    user_id = get_jwt_claims()["user_id"]

    PostLike.query.filter_by(post_id=post_id, user_id=user_id).delete()
    db.session.commit()

    record_last_action(user_id)
    return jsonify(message="Like was deleted"), HTTPStatus.OK


@app.route('/posts/analytics', methods=["Get"])
@jwt_required
def analytics():
    """
    It allows to filter posts needed to be returned by the date.
    :return:  Function returns datetime of post creation and post id and http status
    :rtype: json
    """
    claims = get_jwt_claims()
    date_from = request.args.get('date_from')
    date_to = request.args.get('date_to')
    if date_from and date_to:
        analysis = PostLike.query.filter_by(user_id=claims["user_id"]) \
                    .filter((PostLike.date_liked >= date_from) and
                            (PostLike.date_liked <= date_to)).all()
    elif date_from and date_to is None:
        analysis = PostLike.query.filter_by(user_id=claims["user_id"]) \
            .filter(PostLike.date_liked >= date_from).all()
    elif date_from is None and date_to:
        analysis = PostLike.query.filter_by(user_id=claims["user_id"]) \
            .filter(PostLike.date_liked <= date_to).all()
    else:
        analysis = PostLike.query.filter_by(user_id=claims["user_id"]).all()
    list_of_post_liked = []
    for one_post in analysis:
        list_of_post_liked.append({"datetime": one_post.date_liked, "post_id": one_post.post_id})

    return jsonify({"liked_posts": list_of_post_liked}), HTTPStatus.OK


@app.route('/user/activity', methods=["Get"])
@jwt_required
def user_activity():
    user_id = get_jwt_claims()["user_id"]
    user = User.query.get(user_id)
    return jsonify({"most_recent_logged_in": user.last_logged_in,
                    "most_recent_action": user.last_action}), HTTPStatus.OK


if __name__ == '__main__':
    # db.drop_all()
    db.create_all()
    db.session.commit()
    app.run()
